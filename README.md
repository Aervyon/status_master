# Status Master

An open source HTTP status solution

# Why

Honestly, I made this because I wanted a status page and was bored.

# Config

You will find example projects config located at projects.json, and the example config at config.json.

You can see the config documentation at Config.md, and the project config keys at Projects.md

# Extras

No, I wont help you set this up. The documentation should be enough.

© VoidNulll 2019
