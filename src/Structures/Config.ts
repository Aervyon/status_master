interface CertOptions {
    key?: string | any;
    cert?: string | any;
    requestCert?: boolean;
    ca?: string[] | any[];
}

export interface Options {
    port?: number;
    certOptions?: CertOptions;
}

export interface ActualOptions {
    port: number;
    certOptions?: CertOptions;
}

const optionsBase: ActualOptions = {
    port: 8888,
};

/**
 * @class EvolveConfig
 *
 * Class for generating the Evolve-x config
 *
 * @author Null
 */
class Config implements ActualOptions {
    /**
     * @param {Object<Options>} config=optionsBase The configuration for the app from the user.
     *
     * @property {Number} port  The port the application will use
     * @property {String} url The URL the application will use
     * @property {String} mongoUrl The URL the app will use to connect to mongodb
     * @property {Boolean} signups Whether or not signups are enabled
     * @property {Boolean} apiOnly Whether or not to disable the frontend
     */
    public port: number;

    public certOptions?: CertOptions;

    constructor(config: Options = optionsBase) {
        this.port = config.port || optionsBase.port;
        this.certOptions = {
            key: (config && config.certOptions && config.certOptions.key),
            cert: (config && config.certOptions && config.certOptions.cert),
            ca: (config && config.certOptions && config.certOptions.ca),
            requestCert: (config && config.certOptions && Boolean(config.certOptions.requestCert) ),
        };
        this.verify();
    }

    /**
     * @private
     *
     * Verifies the majority of the Evolve configuration.
     *
     * @returns {Object<ActualOptions>} The finale options.
     */
    private verify(): ActualOptions {
        const maxPort = 65535;
        if (this.port > maxPort || isNaN(Number(this.port) ) ) {
            this.port = 8888;
        }
        return this;
    }
}

export default Config;
