import projects from '../../projects.json';
import { EventEmitter } from 'events';

export interface Project {
    url: string;
    name: string;
    showUrl?: boolean;
    api?: string;
}

export default class Projects extends EventEmitter {
    public projects: any[];

    constructor() {
        super();
        this.projects = projects;
    }

    private _verifyProject(project: Project, projects: Project[]): Project | void {
        if (!project.name) {
            throw Error(`PROJECT CONFIG ERROR - Project is missing name!`);
        }
        if (!project.url) {
            throw Error(`PROJECT CONFIG ERROR - Project ${project.name} is missing a URL!`);
        }
        if (projects.find(pro => pro.url === project.url) ) {
            return;
        }
        if (!project.showUrl) {
            project.showUrl = false;
        }
        return project;
    }

    public verifyProjects(): void {
        const projects = [];
        for (const project of this.projects) {
            const projct = this._verifyProject(project, projects);
            if (projct) {
                projects.push(projct);
            }
        }
        this.projects = projects;
        this.emit('ready');
    }
}
