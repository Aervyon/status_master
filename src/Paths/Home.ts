import Path from '../Structures/Path';
import Base from '../Structures/Base';
import { Response } from 'express';
import { join } from 'path';

class Home extends Path {
    constructor(base: Base) {
        super(base);
        this.label = 'Home';
        this.path = '/';
    }

    execute(req: any, res: Response): Promise<any> {
        const path = join(__dirname, '../Frontend/HTML/Home.html');
        return Promise.resolve(res.sendFile(path) );
    }
}

export default Home;
